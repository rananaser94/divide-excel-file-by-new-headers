import pandas as pd
import openpyxl
# Load the Excel file into a Pandas dataframe
df = pd.read_excel('Book1.xlsx')

# Define the number of consecutive empty rows to use for identifying a new header
min_number_empty_rows = 2
empty_rows=0
# Initialize a list to store the dataframes
dfs = []
start_index = 0
# Loop through each row in the dataframe
for i, row in df.iterrows():
    # Check if the row is empty
    if row.isnull().all():
        # If the row is empty, increment the empty row counter
        empty_rows += 1
    else:
        # If the row is not empty, check if we have encountered enough empty rows to indicate a new header
        if empty_rows >= min_number_empty_rows:
            # If we have encountered enough empty rows, create a new dataframe by selecting the rows between the previous and current non-empty rows
            #NEW HEAR IS THE FIrst ROW

            new_header = df.iloc[start_index].tolist()

            if(start_index == 0):
                df_temp = df.iloc[start_index:i - empty_rows].copy()
                df_temp.columns = df.columns


            else:
                df_temp = df.iloc[start_index + 1:i - empty_rows].copy()
                df_temp.columns = new_header
            df_temp=  df_temp.dropna(axis='columns', how='all')
            dfs.append(df_temp)
            start_index = i
            # Reset the empty row counter
        empty_rows = 0
        # Append the final dataframe

new_header = df.iloc[start_index].tolist()
df_temp = df.iloc[start_index+1:].copy()
df_temp.columns = new_header
df_temp=  df_temp.dropna(axis='columns', how='all')
dfs.append(df_temp)

    # Save each dataframe to a separate Excel file
for i, df in enumerate(dfs):
   df.to_excel(f'output_file_{i}.xlsx', index=False)


